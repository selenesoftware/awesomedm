package main

import (
	"fmt"
	"log"
	"os"
)

var x int

func startX() {
	go func() {
		// cmd := exec.Command("/usr/local/bin/symfony", "")
		// log.Printf("Running command and waiting for it to finish...")
		// output, err := cmd.Output()
		// log.Printf("Command finished with output: %v", string(output))
		// log.Printf("Err msg: %v", err)
		attr := os.ProcAttr{}
		proc, err := os.StartProcess("/usr/local/bin/symfony", []string, *attr)
		if err != nil {
			log.Printf("Err msg: %v", err)
		}
		fmt.Println(proc)
	}()
	for {
		x = x + 1
		fmt.Println(x)
	}
}

func main() {
	startX()
}
